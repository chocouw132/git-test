/* ****************************************************************************
**
**      COPYRIGHT (C) 1987, 1988 MICROSOFT
**
** ****************************************************************************
*
*  Module: dlghyph.c --- contains dialog functions for hyphenation dialog.
*
**
** REVISIONS 111
**
** Date         Who Rel Ver     Remarks
**
** ************************************************************************* */

#include "props.h"
#include "sel.h"
"
#include "sedit.h"
#include "prm.h"
#include "screen.h"
#include "prompt.h"
#include "message.h"
#include "hyph.h"
#include "cmd.h"
#include "field.h"
#include "error.h"
#include "help.h"
#include "rareflag.h"
#include "core.h"

#include "idd.h"
#include "sdmdefs.h"
#include "sdmver.h"
#include "sdm.h"
#include "sdmtmpl.h"
#include "sdmparse.h"

#include "hyphen.hs"
#include "hyphen.sdm"


extern BOOL		vfRecording;
extern char szClsStaticEdit[];
extern struct MERR      vmerr;
extern struct SEL       selCur;
extern int		wwCur;
extern struct WWD	**hwwdCur;
extern CHAR             szEmpty[];
extern struct CHP	vchpFetch;
extern int		vccpFetch;
extern CHAR HUGE	*vhpchFetch;
extern CP		vcpFetch;
extern struct UAB       vuab;
extern struct PREF      vpref;
extern struct SCI       vsci;
extern struct BPTB	vbptbExt;
extern struct HYPFD	vhypfd;
extern struct HYPB    **vhhypb;
extern HCURSOR		vhcArrow;
extern HCURSOR		vhcIBeam;
extern HWND             vhwndApp;
extern HWND             vhwndCBT;

FARPROC			lpprocBlinkCaretSEdit;
FARPROC			lpprocStaticEditWndProc;

HWND			hwndStaticEdit = NULL;


EXPORT PASCAL BlinkCaretSEdit(); /* DECLARATION ONLY */
#define flashIDHyph 0x1059
#define XpFromHypIch(dxp, ich)  (DxpLeftSE(dxp) + ((dxp) * (ich)))

csconst CHAR szOK[] = SzSharedKey("OK",OK);
csconst CHAR szYes[] = SzSharedKey("&Yes",Yes);
#define cchPBTextMax ((sizeof(szOK) > sizeof(szYes)) ? \
							sizeof(szOK) : sizeof(szYes))

#define cchUTextMax cchMaxWord*2

struct UTXD
	{
	int         ichSelFirst;
	int         ichSelLim;
	int         ichLimEffective;
	int         flashID;
	int         fSelOn;
	char        stText[cchUTextMax+1];
};

#define cbUTXD (sizeof(struct UTXD))
#define cwUTXD ((cbUTXD + 1) / 2)

extern CP CpBestHypd();
#ifdef WIN23
extern CHAR *PchLastFit3();
#endif /* WIN23 */

/***************************
*
*  Hyphenation functions:
*      CmdHyphenate(), DlgfHyphenate() and their supporting
*      functions.   Main hyphenation functions are located
*      in cshare\hyphenate.c
*
***************************/

/* ****
*
	Function: CmdHyphenate
*  Author:
*  Copyright: Microsoft 1986
*  Date: 12/12/86
*
*  Description: Menu level command function for "Proof Hyphenate" dialog
*
** ***/

/* %%Function:CmdHyphenate %%Owner:bryanl */
CMD CmdHyphenate(pcmb)
CMB * pcmb;
{
	CABHYPHEN      *pcab;
	CHAR           *szT;
	int             dxaT;
	struct DOP     *pdop;

	TMC		tmc;
	BOOL		fOverflow;
	CHAR            szNum[ichMaxNum + 1];
	CPR		cpr;

	cmdReturn = cmdOK; /* we haven't failed yet */

	if (pcmb->fDefaults)
		{
		CHAR	*pch;

		/* Get the hot zone value from the current dop. */
		pdop = &PdodMother(selCur.doc)->dop;

		if (pdop->dxaHotZ <= 0 || vsci.xaRightMax <= pdop->dxaHotZ)
			{
			pdop->dxaHotZ = dxaInch / 4;
			}
		pch = szNum;
		CchExpZa(&pch, pdop->dxaHotZ, vpref.ut, ichMaxNum, fTrue);
		*pch = '\0';
		if (!FSetCabSz(pcmb->hcab, szNum, Iag(CABHYPHEN, hszHypHotZ)))
			return cmdNoMemory;

		pcab = (CABHYPHEN *) *pcmb->hcab;
		pcab->fHypCaps = vrf.fHyphCapitals;
		pcab->fHypConfirm = fTrue;
		vrf.fHotZValid = fTrue;
		}

	if (pcmb->fDialog || pcmb->fAction)
		{
		char dlt [sizeof (dltHyphenate)];

		if (vhhypb == NULL)
			{	/* do only if not already set up */
			cmdReturn = cmdError;
			if (!FLoadHyph()
					|| (cmdReturn = CmdHyphInit()) != cmdOK)
				goto LReturnUnload;
			}
#ifdef DEBUG
		else
			{
			Assert( vhypfd.fn != fnNil );
			Assert( vhypfd.hrgoutp != hNil );
			}
#endif

		BltDlt(dltHyphenate, dlt);
		if ((tmc = TmcOurDoDlg(dlt, pcmb)) == tmcError)
			{
			cmdReturn = cmdNoMemory;
			goto LReturnCancel;
			}

		if (tmc == tmcCancel)
			{
			cmdReturn = cmdCancelled;
			goto LReturnCancel;
			}

		if (vfRecording)
			{
			FRecordCab(pcmb->hcab, IDDHyphenate, pcmb->tmc, 
					imiHyphenate);
			}

		/* Save the global hyphenation setting for the next time. */
		/* First, protect ourselves from a heap movement.         */
		pcab = (CABHYPHEN *) *pcmb->hcab;
		vrf.fHyphCapitals = pcab->fHypCaps;

		GetCabSz(pcmb->hcab, szNum, ichMaxNum,
				Iag(CABHYPHEN, hszHypHotZ));
		if (FZaFromSs(&dxaT, szNum, CchSz(szNum) - 1,
				vpref.ut, &fOverflow) &&
				dxaT > 0)
			{
			PdodMother(selCur.doc)->dop.dxaHotZ = dxaT;
			}
		else  if (vrf.fHotZValid)
			{
			/* Haven't warned the user yet about this invalid
				hot zone measurement. */
			ErrorEid(eidInvalidHotZ, " CmdHyphenate");
			cmdReturn = cmdError;
			goto LReturnCancel;
			}

		if (tmc == tmcHypChange)
			{
			HyphChangeSelection(dxaT, vrf.fHyphCapitals, &cpr);
			/* Because of unmatching StartLongOp() in
				HyphChangeSelection(). */
			EndLongOp(fFalse /* fAll */);
			}
		cmdReturn = cmdOK;
LReturnCancel:  
		HyphCancel();
LReturnUnload:	
		UnloadHyph();
		}

	return cmdReturn;
}


/******
*
*  Function: FDlgHyphenate
*  Author:
*  Copyright: Microsoft 1986
*  Date: 12/12/86
*
*  Description: Dialog box function for "Proof Hyphenate" dialog
*
******/

/* %%Function:FDlgHyphenate %%Owner:bryanl */
BOOL FDlgHyphenate(dlm, tmc, wNew, wOld, wParam)
DLM	dlm;
TMC	tmc;
WORD	wNew, wOld, wParam;
{
	int          dxaT;
	struct HYPD *phypd;
	CHAR         szT[cchPBTextMax];
	struct UTXD  utxd;
	HWND hwnd;
	struct RC rcDlg, rcApp;
	int dyp;

	
	switch (dlm)
		{
	case dlmIdle:
		if (wNew /* cIdle */ == 0)
			return fTrue;  /* call FSdmDoIdle and keep idling */
		if (wNew /* cIdle */ == 3)
			FAbortNewestCmg (cmgUtilEdit, fTrue, fTrue);
		else  if (wNew /* cIdle */ == 5)
			FAbortNewestCmg (cmgPromptUtilAC, fTrue, fTrue);
		else  if (wNew > 5)
			return fTrue; /* stop idling */
		return fFalse; /* keep idling */

	case dlmInit:
		if (FIsDlgDying())
			return fTrue;   /* just get out. Dlg will come down  bz */
	/* position the dialog out of the way */
		GetClientRect(vhwndApp, (LPRECT) &rcApp);
		hwnd = HwndFromDlg(HdlgGetCur());
		GetWindowRect(hwnd, (LPRECT) &rcDlg);
		dyp = rcDlg.ypBottom - rcDlg.ypTop;
		ScreenToClient(vhwndApp, (LPPOINT) &rcDlg.ptTopLeft);
		MoveDlg(rcDlg.xpLeft, rcApp.ypBottom - dyp - vsci.dypBdrCapt);
		Assert( vhhypb != hNil );
		SetHyphStaticEdit(NULL);

		/* if selection exactly one word, do hyphenation and display it */
		if ((*vhhypb)->fHyphWord)
			{
			FreezeHp();
			FetchToHypd(selCur.doc, selCur.cpFirst, selCur.cpLim,
					&(*vhhypb)->hypd);
			HyphenateWord(&(*vhhypb)->hypd, vrf.fHyphCapitals);
			(*vhhypb)->hypd.ichLimEffective = -1;
			if ((*vhhypb)->hypd.iichMac > 0)
				{
				(*vhhypb)->hypd.iichBest = (*vhhypb)->hypd.iichMac-1;
				if (FHypdWidow(&(*vhhypb)->hypd))
					goto LNoBest;
				}
			else
				{
LNoBest:		
				(*vhhypb)->hypd.iichBest = -1;
				}
			MeltHp();
			EnableTmc(tmcHypNoChange, fFalse);
			EnableTmc(tmcHypConfirm, fFalse);
			vrf.fHyphHaveWord = fTrue;
			goto LDispWord;
			}

	/* We want to hyphenate the entire document/selection. */

		/* Set up push buttons to initial state. */
		EnableTmc(tmcHypNoChange, fFalse);
		CchCopyLpszCchMax(szOK, (CHAR FAR *) szT, cchPBTextMax);
		SetTmcText(tmcHypChange, szT);
		EnableTmc(tmcHypAt, fFalse);
		vrf.fHyphHaveWord = fFalse;
		break;

	case dlmClick:
		switch (tmc)
			{
			CP 	cp;
			int	cHyphen;
			int	ichLast;
			CHAR	*pchCur;
			HCAB hcab;

		case tmcHypChange:

			if (!vrf.fHyphHaveWord)
				goto LNextWord;	/* don't have a word yet, go scan for one */

			DelHyphHhypb(vhhypb);
			SendMessage(hwndStaticEdit, WM_COMMAND,	SEM_GETDSCRP, (LPSTR) &utxd);
			Assert( utxd.ichSelFirst > 0 );
			for (pchCur = &(utxd.stText[1]), cHyphen = ichLast = 0;
					ichLast < utxd.ichSelFirst;  ichLast++, pchCur++)
				{
				if (*pchCur == '-')
					cHyphen++;
				}
			phypd = &((*vhhypb)->hypd);
#ifdef BRYANL
			CommSzNum( SzShared( "Insert Hyph at cp = " ), 
					(int) (phypd->rgdcp[utxd.ichSelFirst - cHyphen] + phypd->cpFirst) );
#endif
			HyphInsertCp( phypd->rgdcp[utxd.ichSelFirst - cHyphen]
					+ phypd->cpFirst);
			Debug(phypd = 0); /* can't assume no HM */

			if (vmerr.fDiskFail || vmerr.fMemFail)
			/* if failure, dialog will be taken down without EndDlg */
				return (fTrue);

LMaybeNextWord:	
			if ((*vhhypb)->fHyphWord)
				goto LEndTmc;

LNextWord:	
			if (!FGetHotZoneWidth(tmcHypHotZ, &dxaT))
				{
				SetTmcTxs(tmcHypHotZ, TxsAll());
				break;
				}
			if (!ValGetTmc(tmcHypConfirm))
				{
LEndTmc:
				if ((hcab = HcabFromDlg(fFalse)) == hcabNotFilled)
					return fFalse;
				if (hcab != hcabNull)
					{
					Assert(PcmbDlgCur()->hcab == hcab);
					EndDlg(tmc);  /* ok to call EndDlg here */

					}
			/* if hcabNull, dialog will be taken down without EndDlg */
				return (fTrue);
				break;
				}
			if (!vrf.fHyphHaveWord)
				{
				vrf.fHyphHaveWord = fTrue;
				EnableTmc(tmcHypNoChange, fTrue);
				CchCopyLpszCchMax(szYes, (CHAR FAR *) szT, cchPBTextMax);
				SetTmcText(tmcHypChange, szT);
				}
			vrf.fHyphCapitals = ValGetTmc(tmcHypCaps);
LRetry:
			if (!FHyphFind(dxaT, fFalse, vrf.fHyphCapitals, NULL))
				{
				if ((hcab = HcabFromDlg(fFalse)) == hcabNotFilled)
					return fFalse;
				if (hcab != hcabNull)
					{
					Assert(PcmbDlgCur()->hcab == hcab);
					EndDlg(tmcHypNoChange);   /* ok to call EndDlg here */

					}
			/* if hcabNull, dialog will be taken down without EndDlg */
				return (fTrue);
				}

			cp = CpBestHypd(&(*vhhypb)->hypd);
/* calculate effective hyphenation limit */
			phypd = &(*vhhypb)->hypd;
			if (phypd->ichLimEffective <= 0)
				{
				IchLimEffectiveHypd();
				}
			Debug(phypd = 0); /* can't assume no HM */
LDispWord:	
			ProposeHyph(vhhypb);
			EnableTmc(tmcHypAt, fTrue);
			SetHyphStaticEdit(vhhypb);

			if ((*vhhypb)->hypd.iichBest < 0)
				{
/* hyphenation's opinion is that we should not hyphenate this word.  However,
	if we just selected a word and asked hyphenate to break it down, we want
	to offer the option anyway.  Hence this code. */
				SendMessage(hwndStaticEdit, WM_COMMAND,
						SEM_GETDSCRP, (LPSTR) &utxd);
				SetDefaultTmc( (*vhhypb)->fHyphWord ?
						tmcCancel :
						tmcHypNoChange );
				if ((*vhhypb)->fHyphWord && utxd.ichSelFirst > 0)
					{
					EnableTmc( tmcHypChange, fTrue );
					if (FEnabledTmc(tmcHypText))
						SetFocusTmc(tmcHypText);
					}
				else
					EnableTmc( tmcHypChange, fFalse );
				break;
				}
/* have both suggested and recommended hyphenation points for this word */
			EnableTmc( tmcHypChange, fTrue );
			SetDefaultTmc( tmcHypChange );
			if (FEnabledTmc(tmcHypText))
				SetFocusTmc(tmcHypText);
			break;

		case tmcHypNoChange:
			Assert( vrf.fHyphHaveWord );
			DelHyphHhypb(vhhypb);
			goto LMaybeNextWord;
			}	/* end switch (tmc) */
		}	/* end switch (dlm) */
	return fTrue;
}



/******
*
*  Function: FGetHotZoneWidth
*  Author:
*  Copyright: Microsoft 1986
*  Date: 12/12/86
*
*  Description: Get a dxa value from the hot zone edit control.
*               Returns fTrue iff the hot zone value is correct.
*
******/

/* %%Function:FGetHotZoneWidth %%Owner:bryanl */
FGetHotZoneWidth(tmc, pdxa)
int     tmc;
int    *pdxa;
{
	int         cch;
	BOOL	fOverflow;
	CHAR        szNum[ichMaxNum];

	cch = CchGetTmcText(tmc, szNum, ichMaxNum - 1);
	if (cch != 0)
		{
		if (FZaFromSs(pdxa, szNum, cch, vpref.ut, &fOverflow)
				&& !fOverflow
				&& *pdxa > dxaHotZMin && *pdxa < vsci.xaRightMax)
			{
			vrf.fHotZValid = fTrue;
			return fTrue;
			}
		}
LInvalid:
	ErrorEid(eidInvalidHotZ, "FGetHotZoneWidth");
	vrf.fHotZValid = fFalse;
	return (fFalse);
}
